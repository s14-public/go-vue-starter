export default {
  install: (Vue) => {
    Vue.prototype.$helpers = {

      ////////////////////////////////////////////////////////////////////
      // url locations  - loaded from .env.* files ///////////////////////
      ////////////////////////////////////////////////////////////////////
      api() {
        return process.env.VUE_APP_API
      },

      urlRoute() {
        return process.env.VUE_APP_URL
      },

      ////////////////////////////////////////////////////////////////////
      // currency functions //////////////////////////////////////////////
      ////////////////////////////////////////////////////////////////////

      encodeCurrency(amt) {
        //this takes a string and returns it as a float
        //that's limited to two decimal places
        let floatStr = parseFloat(amt).toFixed(2)
        let finalAmt = parseFloat(floatStr)
        return finalAmt
      },

      encodeCents(amt) {
        //takes a two decimal place dollar amount and
        //converts it to cents for database storage
        let mult = amt * 100
        let fl = parseFloat(mult).toFixed(2)
        let enc = Math.round(fl)
        return enc
      },

      parseCents(amt) {
        //converts cents to dollars
        let div = amt / 100
        let par = parseFloat(div).toFixed(2)
        return par
      },

      ////////////////////////////////////////////////////////////////////
      // date functions //////////////////////////////////////////////////
      ////////////////////////////////////////////////////////////////////

      formatDate(d) {
        //takes a unix time stamp and convers it to
        //a human readable string. ex. 10-5-18
        let date = new Date(d)
        let day = date.getDate()
        let month = date.getMonth() + 1 //why?
        let year = date.getFullYear().toString().substr(2, 2);
        let dateStr = month + "-" + day + "-" + year

        return dateStr
      },

      ////////////////////////////////////////////////////////////////////
      // auth token functions ////////////////////////////////////////////
      ////////////////////////////////////////////////////////////////////
      
      setToken(t) {
        localStorage.setItem('jwt',t.data)
      },

      getToken() {
        return localStorage.getItem('jwt')
      },

			 ////////////////////////////////////////////////////////////////////
      // ajax requests ////////////////////////////////////////////////////
      /////////////////////////////////////////////////////////////////////
      
			async postData(url = '', req = {}) {
				const res = await fetch(url, {
					method: 'POST',
					mode: 'cors',
					cache: 'no-cache',
					credentials: 'same-origin',
					headers: {
						'Content-Type': 'application/json',
						'X-Requested-With': 'XMLHttpRequest'
						},
					redirect: 'follow',
					referrerPolicy: 'no-referrer',
					body: JSON.stringify(req)
				});
				
				let data = {}
				
				//todo: handle responses that aren't json
				if (!res.ok) {
					throw new Error(`Post error: ${res.status}`)
				} else {
					data = await res.json()
				}

				return data
			},

      ////////////////////////////////////////////////////////////////////
      // browser window size /////////////////////////////////////////////
      ////////////////////////////////////////////////////////////////////
      pageSize() {
        let windowSize = window.innerWidth
        return windowSize
      },

      ////////////////////////////////////////////////////////////////////
      // generate a multi-dimensional array to create rows easier ////////
      ////////////////////////////////////////////////////////////////////
      generateRows(data,cols) {
        let res = []
        let row = []

        for (var i=0; i<data.length; i++) {
          row.push(data[i])

          if ((i+1) % cols == 0 || i == (data.length-1)) {
            res.push(row)
            row = []
          }
        }

        return res
      },

      ////////////////////////////////////////////////////////////////////
      // string manipulation /////////////////////////////////////////////
      ////////////////////////////////////////////////////////////////////
      removeDashes(data) {
        let split = data.split('-')
        let joined = split.join('')

        return joined
      },

      getSplitUrl(data) {
        let split = data.split('/')
        return split
      }

    };
  }
};

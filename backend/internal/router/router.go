package router

import (
	"log"
	"net"
	"net/http"
	"time"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"gitlab.com/s14-public/go-vue-starter/backend/config"
	"gitlab.com/s14-public/go-vue-starter/backend/internal/controller"
	"gitlab.com/s14-public/go-vue-starter/backend/internal/logger"
	"gitlab.com/s14-public/go-vue-starter/backend/internal/model"
)

func StartServer() {
	//build app config
	env := &config.Env{}
	env.Build("config.json")

	r := mux.NewRouter()

	//headers
	headersOk := handlers.AllowedHeaders([]string{"X-Requested-With", "Content-Type", "User-Token"})
	methodsOk := handlers.AllowedMethods([]string{"GET", "HEAD", "POST", "PUT", "OPTIONS"})

	//cors handling - limit the ip's that can hit this here
	//currently set to all
	originsOk := handlers.AllowedOrigins([]string{"*"})

	//setup routes
	routes(r, env)

	//set up listener
	l, err := net.Listen("tcp4", env.GetPort())
	if err != nil {
		log.Fatal(err)
	}

	//config server
	server := &http.Server{
		Handler:      handlers.CORS(originsOk, headersOk, methodsOk)(r),
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 60 * time.Second,
	}

	//launch server
	log.Fatal(server.Serve(l))
}

type WidgetServer struct {
	service controller.WidgetService
	env     config.Config
}

func NewWidgetServer(service controller.WidgetService) *WidgetServer {
	return &WidgetServer{service: service}
}

func routes(r *mux.Router, env *config.Env) {
	//configure widget
	widgetModel := &model.Widget{}
	widgetTx := &model.Tx{}
	service := controller.NewWidgetController(widgetModel, widgetTx, env)
	widgetServer := NewWidgetServer(service)

	api := r.PathPrefix("/api").Subrouter()
	api.HandleFunc("/widget", widgetServer.GetWidgets).Methods("POST")
	api.HandleFunc("/widget/{id}", widgetServer.GetWidget).Methods("GET")
	api.HandleFunc("/widget/create", widgetServer.CreateWidget).Methods("POST")
	api.HandleFunc("/widget/delete", widgetServer.DeleteWidget).Methods("DELETE")
}

func (server *WidgetServer) GetWidgets(w http.ResponseWriter, r *http.Request) {
	widgets, err := server.service.GetWidgets(r)
	if err != nil {
		log.Println(err)
		//logger.WriteError(logger.General, "error getting widgets: ", err)
		w.WriteHeader(http.StatusBadRequest)
	} else {
		w.Write(widgets)
	}
}

func (server *WidgetServer) GetWidget(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["id"]

	widget, err := server.service.GetWidget(id)
	if err != nil {
		server.env.WriteError(logger.General, "error getting widget: ", err)
		w.WriteHeader(http.StatusBadRequest)
	} else {
		w.Write(widget)
	}
}

func (server *WidgetServer) CreateWidget(w http.ResponseWriter, r *http.Request) {
	err := server.service.CreateWidget(r)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
	} else {
		w.WriteHeader(http.StatusOK)
	}
}

func (server *WidgetServer) DeleteWidget(w http.ResponseWriter, r *http.Request) {
	err := server.service.DeleteWidget(r)
	if err != nil {
		server.env.WriteError(logger.General, "error deleting widget: ", err)
		w.WriteHeader(http.StatusBadRequest)
	} else {
		w.WriteHeader(http.StatusOK)
	}
}

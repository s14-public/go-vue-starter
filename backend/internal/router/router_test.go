package router

import (
	"net/http"
	"net/http/httptest"
	"testing"
	//"gitlab.com/s14-public/go-vue-starter/backend/internal/controller"
)

type MockWidget struct{}

func (w *MockWidget) GetWidgets(r *http.Request) ([]byte, error) {
	widgets := []byte(string("widgets"))
	return widgets, nil
}

func (w *MockWidget) GetWidget(idStr string) ([]byte, error) {
	widget := []byte(string("widget"))
	return widget, nil
}

func (w *MockWidget) CreateWidget(r *http.Request) error {
	return nil
}

func (w *MockWidget) DeleteWidget(r *http.Request) error {
	return nil
}

func TestGetWidgets(t *testing.T) {
	service := &MockWidget{}
	widgetServer := NewWidgetServer(service)

	req := httptest.NewRequest(http.MethodPost, "/api/widget", nil)
	res := httptest.NewRecorder()

	widgetServer.GetWidgets(res, req)
}

func TestGetWidget(t *testing.T) {
	service := &MockWidget{}
	widgetServer := NewWidgetServer(service)

	req := httptest.NewRequest(http.MethodPost, "/api/widget/5", nil)
	res := httptest.NewRecorder()

	widgetServer.GetWidget(res, req)
}

func TestCreateWidget(t *testing.T) {
	service := &MockWidget{}
	widgetServer := NewWidgetServer(service)

	req := httptest.NewRequest(http.MethodPost, "/api/widget/create", nil)
	res := httptest.NewRecorder()

	widgetServer.CreateWidget(res, req)
}

func TestDeleteWidget(t *testing.T) {
	service := &MockWidget{}
	widgetServer := NewWidgetServer(service)

	req := httptest.NewRequest(http.MethodPost, "/api/widget/delete", nil)
	res := httptest.NewRecorder()

	widgetServer.DeleteWidget(res, req)
}

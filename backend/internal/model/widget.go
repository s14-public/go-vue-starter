package model

import (
	"database/sql"
	"strings"
	"time"

	"gitlab.com/s14-public/go-vue-starter/backend/config"
	"gitlab.com/s14-public/go-vue-starter/backend/internal/logger"
)

type Widget struct {
	Id           int64     `json:"id"`
	Name         string    `json:"name"`
	Color        string    `json:"color"`
	Value        int       `json:"value"`
	DateModified time.Time `json:"date_modified"`
}

type WidgetManager interface {
	FindAll(r interface{}, env config.Config) ([]Widget, error)
	Find(id int, env config.Config) error
	Create(t *Tx) (sql.Result, error)
	Update(t *Tx) (sql.Result, error)
	Delete(t *Tx, id int) error
}

//struct used for setting sql search params
type WidgetParams struct {
	Id    sql.NullInt64
	Name  sql.NullString
	Color sql.NullString
}

//get a slice of widgets based around n number of
//search parameters
func (w *Widget) FindAll(r interface{}, env config.Config) ([]Widget, error) {
	db := env.GetDb()
	var widgets []Widget

	//build parameters for a sql search
	var params WidgetParams
	params.buildWhereParams(r)

	var q string = `SELECT * FROM widget WHERE
		($1::bigint is NULL or id = $1) AND
		($2::text is NULL or LOWER(name) LIKE '%' || $2 || '%') AND
		($3::text is NULL or LOWER(color) LIKE '%' || $3|| '%')`

	//run sql query
	stmt, err := db.Prepare(q)
	if err != nil {
		return widgets, err
	}
	defer stmt.Close()

	rows, err := stmt.Query(params.Id, params.Name, params.Color)
	if err != nil {
		return widgets, err
	}
	defer rows.Close()

	//scan returned rows into a slice
	for rows.Next() {
		var w Widget

		err = rows.Scan(&w.Id, &w.Name, &w.Color, &w.Value, &w.DateModified)
		if err != nil {
			if err == rows.Err() {
				env.WriteError(logger.General, "error querying widget: ", err)
			}
		}

		widgets = append(widgets, w)
	}

	return widgets, nil
}

//get a single widget based on widget id
//
//todo: should you just return a Widget here? seems redundant
//but the controller calling this function is passing a pointer
//which could cause problems potentially?
func (w *Widget) Find(id int, env config.Config) error {
	db := env.GetDb()

	q := `SELECT * FROM widget WHERE id=$1`

	stmt, err := db.Prepare(q)
	if err != nil {
		return err
	}
	defer stmt.Close()

	err = stmt.QueryRow(id).Scan(&w.Id, &w.Name, &w.Color, &w.Value, &w.DateModified)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil
		}

		return err
	}

	return nil
}

func (w *Widget) Create(t *Tx) (sql.Result, error) {
	var result sql.Result

	q := `INSERT INTO widget(name,color,value,date_modified) VALUES($1,$2,$3,$4)`

	stmt, err := t.tx.Prepare(q)
	if err != nil {
		return result, err
	}
	defer stmt.Close()

	result, err = stmt.Exec(w.Name, w.Color, w.Value, time.Now())
	if err != nil {
		return result, err
	}

	return result, nil
}

func (w *Widget) Update(t *Tx) (sql.Result, error) {
	var result sql.Result

	q := `UPDATE widget SET name=$2, color=$3, value=$4 
	date_modified=$5 WHERE id=$1`

	stmt, err := t.tx.Prepare(q)
	if err != nil {
		return result, err
	}
	defer stmt.Close()

	result, err = stmt.Exec(w.Name, w.Color, w.Value, time.Now())
	if err != nil {
		return result, err
	}

	return result, nil
}

func (w *Widget) Delete(t *Tx, id int) error {
	q := `DELETE FROM widget where id=$1`

	stmt, err := t.tx.Prepare(q)
	if err != nil {
		return err
	}
	defer stmt.Close()

	_, err = stmt.Exec(id)
	if err != nil {
		return err
	}

	return nil
}

//this function builds search parameters for a database query.
//the data is the request body (json) of an http request
//
//we step through each key, and check it's value.
//if the value is set we populate it, otherwise we return the
//null version.
//
//Using this technique, and a postgres db, we can write a query
//with many parameter combinations. If a parameter isn't set, the
//database simply ignores it.
func (p *WidgetParams) buildWhereParams(r interface{}) {
	//singular parameters to check
	if m, ok := r.(map[string]interface{}); ok {
		if m["id"] != nil && m["id"] != "" && m["id"] != "0" {
			p.Id = GetNullInt64FromString(m["id"], true)
		} else {
			p.Id = GetNullInt64(0, false)
		}

		if m["name"] != nil && m["name"] != "" {
			lowecase := strings.ToLower(m["name"].(string))
			p.Name = GetNullString(lowecase, true)
		} else {
			p.Name = GetNullString("", false)
		}

		if m["color"] != nil && m["color"] != "" {
			lowecase := strings.ToLower(m["color"].(string))
			p.Color = GetNullString(lowecase, true)
		} else {
			p.Color = GetNullString("", false)
		}
	}
}

package controller

import (
	"encoding/json"
	"net/http"
	"strconv"

	"gitlab.com/s14-public/go-vue-starter/backend/config"
	"gitlab.com/s14-public/go-vue-starter/backend/internal/model"
)

//add another field to WidgetController named tx that is a
//the model.Tx interface. You need to mock a method that
//returns a new sql.Tx instance

type WidgetController struct {
	service model.WidgetManager
	tx      model.Txer
	env     config.Config
}

type WidgetService interface {
	GetWidgets(r *http.Request) ([]byte, error)
	GetWidget(idStr string) ([]byte, error)
	CreateWidget(r *http.Request) error
	DeleteWidget(r *http.Request) error
}

//todo: make a struct for this? it's too wide, and looks bad stacked like this
func NewWidgetController(
	service model.WidgetManager,
	tx model.Txer,
	env *config.Env) *WidgetController {

	tx.New(env.GetDb())

	return &WidgetController{
		service: service,
		tx:      tx,
		env:     env}
}

func (wc *WidgetController) GetWidgets(r *http.Request) ([]byte, error) {
	var jsonData []byte

	//decode request into an interface type to pass
	//as search parameters in our query
	body, err := GetRequestBody(r)
	if err != nil {
		return jsonData, err
	}

	//query database
	widgets, err := wc.service.FindAll(body, wc.env)
	if err != nil {
		return jsonData, err
	}

	//return data
	jsonData, err = json.Marshal(widgets)
	if err != nil {
		return nil, err
	}

	return jsonData, nil
}

func (wc *WidgetController) GetWidget(idStr string) ([]byte, error) {
	var jsonData []byte

	id, err := strconv.ParseInt(idStr, 10, 64)
	if err != nil {
		return jsonData, err
	}

	err = wc.service.Find(int(id), wc.env)
	if err != nil {
		return jsonData, err
	}

	//format as json
	jsonData, err = json.Marshal(wc.service)
	if err != nil {
		return jsonData, err
	}

	return jsonData, nil
}

func (wc *WidgetController) CreateWidget(r *http.Request) error {
	//decode request
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&wc.service)
	if err != nil {
		return err
	}

	//start db transaction
	//db := wc.env.GetDb()
	//tx, err := db.Begin()

	//begin new db transaction
	err = wc.tx.Begin()
	if err != nil {
		return err
	}

	//create widget
	_, err = wc.service.Create(wc.tx.Get())
	if err != nil {
		return err
	}

	//handle commit or data rollback
	err = wc.tx.Commit()
	if err != nil {
		return err
	}

	return nil
}

func (wc *WidgetController) DeleteWidget(r *http.Request) error {
	type Body struct {
		Id int `json:"id"`
	}

	var body Body

	//decode request
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&body)
	if err != nil {
		return err
	}

	//start db transaction
	err = wc.tx.Begin()
	if err != nil {
		return err
	}

	err = wc.service.Delete(wc.tx.Get(), body.Id)
	if err != nil {
		return err
	}

	//handle commit or data rollback
	err = wc.tx.Commit()
	if err != nil {
		return err
	}

	return nil
}

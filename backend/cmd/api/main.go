package main

import (
	"gitlab.com/s14-public/go-vue-starter/backend/internal/router"
)

func main() {
	router.StartServer()
}

module gitlab.com/s14-public/go-vue-starter/backend

go 1.14

require (
	github.com/gorilla/handlers v1.4.2
	github.com/gorilla/mux v1.7.4
	github.com/lib/pq v1.7.0
	gitlab.com/s14-public/logger v1.0.0
)

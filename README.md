# Go + VueJs + Postgres Example Project

This example project is a simple web application built with VueJs (2.x)
as a frontend, Go (1.x) as a backend api, and a Postgres database. This is not a
microservice based api, but something that is more monolithic in nature. I've 
created this to be as simple as possible, while still demonstrating how to structure
a project using these tools.

Although I use the term "web application", I'm using this project structure
to build internal, browser based business applications. 

## Requirements:

- Go 1.13 or later
- Postgres 9.x
- NPM (I would use nvm)
- VueCli tool (available through npm)

## Installation:
- Clone this repository
- Create a postgres database named `go_vue`
- Run the `.sql` file in `/schemas` to setup a widget table

## Configuration:
- Open `/frontend/.env.example` and save as `.env.local`
- Change parameters in `env.local` to match your system
- Open `/backend/cmd/api/config.json` and change parameters to match your system
- Add your database password as an environment variable `export DB_PASS=yourpassword`.
If you don't want to do it that way, you'll need to modify `dbpassword` in `/backend/config/config.go`

## Run the application:
- Run the frontend with `npm serve go-vue-starter/frontend`
- Build the backend with `go build -o api /go-vue-starter/backend/cmd/api/main.go`
- Run the backend with `./api`

## Overview:

**This project lets users create, delete, and fetch "widgets".**

### Frontend

The two main components which drive the user interface are `CreateWidget` and
`ListWidgets`. You can see how they communicate with a parent inside of `Home.vue`.

There is a router setup, so you can see the basic functionality.

### Backend

The backend's data flows from a router -> controller -> model. Although I'm using
the terms model and controller, I'm not using a MVC architecture per se. The `model` 
package models database table structure, and provide's CRUD functions to access data.

In this project, the `controller` is mainly parsing http requests and marshaling JSON
to send as a response. In a more robust application, controllers handle: user authentication, 
chaining transactional queries, and more advanced logic. While there are plenty of 
other ways to structure applications, I've had success with this method for small and
medium sized apps.

Finally the `router` is simply handling routes, and passing back whatever the controller
has returned. I think it looks cleaner without much logic in the handlers. And, with all the
routes in one place, I can assess what's happening easier.















